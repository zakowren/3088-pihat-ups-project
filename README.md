# 3088 PiHat UPS Project 

A (Uninterruptible)UPS hat that will prevent data loss and damage to the pi, especially with load-shedding in South Africa. The device will have and LED that displays battery full, battery running low, and charging status. The design will need to convert the battery Voltage to the correct voltage for the pi (5V). Optional automatic device shutdown.

To use the device, simply attach to pi, and connect power cable to unit. The device will charge while the device is plugged in, and allow the device to remain active while no alternative power source is avalible.
